﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_34
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello how many weeks would you like to know the working days of?");
            var i = int.Parse(Console.ReadLine());
            var week = 1;

            do
            {
                var b = ((i * 7) / 7) * 5;

                Console.WriteLine($"There are {b}");

                i++;

            } while (i < week);
         }
    }
}
