﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello please type any word you would like");
            string temp = Console.ReadLine();
            int letters = 0;

            foreach (Char c in temp)
            {
                if (char.IsLetter(c))
                {
                    letters += 1;
                }
            }

            Console.WriteLine("This word has a charachter count of: " + letters);
            Console.ReadLine();
        }
    }
}
