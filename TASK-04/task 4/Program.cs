﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("hello would you like to convert Fahrenheit to Celsius \"f\" or Celsius to Fahrenheit \"c\" ?");

            var convert = Console.ReadLine();
            

            switch (convert)
            {
                case "f":
                    Console.WriteLine("What Fahrenheit would you lke to convert to Celsius? plese enter:");
                    var f2c = double.Parse(Console.ReadLine());
                    var fahconvert = System.Math.Round((5.0 / 9.0) * (f2c - 32)); 
                    Console.WriteLine($"{f2c} fahrenheit is eqaul to {fahconvert} celsius");
                    break;

                case "c":
                    Console.WriteLine("What Celsius would you lke to convert to Farenheit? plese enter:");
                    var c2f = double.Parse(Console.ReadLine());
                    var celconvert = System.Math.Round(((9.0 / 5.0) * c2f) + 32);
                    Console.WriteLine($"{c2f} Celsius is eqaul to {celconvert} Farenheit");
                    break;

                default:
                    Console.WriteLine("sorry please enter a valid option");
                    break;
            }
            }
    }
}
