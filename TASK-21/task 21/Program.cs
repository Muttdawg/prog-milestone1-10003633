﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_21
{

    class Program
    {
        static void Main()
        {
            Dictionary<string, int> values =
                new Dictionary<string, int>();

            values.Add("January", 31);
            values.Add("February", 30);
            values.Add("March", 31);
            values.Add("April", 30);
            values.Add("May", 31);
            values.Add("June", 30);
            values.Add("July", 31);
            values.Add("August", 31);
            values.Add("September", 30);
            values.Add("October", 31);
            values.Add("November", 30);
            values.Add("December", 31);
            
            string answer;
            if (values.TryGetValue(31, out answer))
            {
                Console.WriteLine(answer);
            }
            if (values.TryGetValue(30, out answer))
            {
                Console.WriteLine(false);
            }
        }
    }
    }
        

        
    

